	Check ver:
git --version

	For config:
git config --global user.name ""
git config --global user.email ""
git config --list

	For Branch
git branch
git checkout

	For repository:
git init                                     //       can use with <file> this create new folder with name is "file"
git status 
git add .				     // can with <file> 
git commit -m ""
git push 
git remote add origin https://gitlab.com/2yxa/projecttestgit.git
git push -u origin master
git clone git://github.com/schacon/grit.git			//clone repo

	For .gitignor
# ����������� � ��� ������ ������������
# �� ������������ �����, ��� ������� ������������� �� .a
*.a
# �� ����������� ���� lib.a, �������� �� ��, ��� �� ���������� ��� .a ����� � ������� ����������� �������
!lib.a
# ������������ ������ ���� TODO ����������� � �������� ��������, �� ��������� � ������ ���� subdir/TODO
/TODO
# ������������ ��� ����� � �������� build/
build/
# ������������ doc/notes.txt, �� �� doc/server/arch.txt
doc/*.txt
# ������������ ��� .txt

	For git lfs
git lfs install    			//for check
.pbxproj -crlf -diff			// or //     *.pbxproj binary

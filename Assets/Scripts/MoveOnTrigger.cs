﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MoveOnTrigger : MonoBehaviour
{
    public GameObject point;
    public static bool mark = false;
    // Use this for initialization
    void Start()
    {
    }


    private void OnTriggerEnter(Collider collider)
    {
        print("a");
        if (collider.GetComponent<Animator>())
        {
            mark = true;
            collider.transform.SetParent(null);
            print("b");
            StartCoroutine(Mover(collider));
            collider.transform.SetParent(null);
        }
    }


    IEnumerator Mover(Collider collider)
    {
        while ((collider.transform.position - point.transform.position).sqrMagnitude > 0.02f)
        {
            print("c");
            collider.transform.position = Vector3.Lerp(collider.transform.position, point.transform.position, Time.deltaTime*2f);
            yield return null;
        }
        collider.GetComponent<Animator>().enabled = true;
        mark = false;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}

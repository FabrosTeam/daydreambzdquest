﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventListener : MonoBehaviour {
    Renderer render;
    Vector3 startPosition;
	// Use this for initialization
	void Start ()
    {
        render = gameObject.GetComponent<Renderer>();
        startPosition = gameObject.transform.position;
	}

    public void OnEnter()
    {
        render.material.color = Color.red;
    }
    public void OnExit()
    {
        render.material.color = Color.white;
    }
    public void OnGrab()
    {
        

        if (MoveObjectOnTrigger.mark == false)
        {

            gameObject.GetComponent<Rigidbody>().useGravity = false;
            Transform pointerTransform = GvrPointerInputModule.Pointer.transform;
            transform.SetParent(pointerTransform, true);
            
       }
    }
    public void OnRelase()
    {
        if (MoveObjectOnTrigger.mark == false)
        {
            transform.SetParent(null, false);
            gameObject.GetComponent<Rigidbody>().useGravity = true;
           transform.localPosition = startPosition;
        }
    }
    public void Update()
    {
        
    }
}

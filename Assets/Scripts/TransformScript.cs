﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TransformScript : MonoBehaviour {

    public float X;
    public float Y;
    public float Z;

   public event UnityAction Event = null; 
    void Start () {
        Event += TransformPosition;
        
	}

    public void TransformPosition()
    {
        gameObject.transform.position = new Vector3(X,Y,Z);
    }
}

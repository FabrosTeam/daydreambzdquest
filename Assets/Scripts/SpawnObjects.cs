﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour
{
    public GameObject fire;
    public void Spawn()
    {
        Instantiate<GameObject>(fire, gameObject.GetComponentInChildren<Transform>().transform.position,Quaternion.identity);
    }
}
	

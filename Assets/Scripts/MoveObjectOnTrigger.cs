﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MoveObjectOnTrigger : MonoBehaviour
{
    public GameObject point;
    public static bool mark = false;
    // Use this for initialization
    void Start()
    {
    }


    private void OnTriggerEnter(Collider collider)
    {
        GameObject parent = collider.transform.parent.gameObject;
        if (parent.GetComponent<Animator>())
        {
            mark = true;
            StartCoroutine(Mover(parent));
            parent.transform.SetParent(null);
        }
    }


    IEnumerator Mover(GameObject parent)
    {
        while ((parent.transform.position - point.transform.position).sqrMagnitude > 0.3f)
        {
            parent.transform.position = Vector3.Lerp(parent.transform.position, point.transform.position, Time.deltaTime*3f);
            parent.transform.rotation = Quaternion.Lerp(parent.transform.rotation, point.transform.rotation, Time.deltaTime * 3f);
            yield return null;
        }
        parent.GetComponent<Animator>().enabled = true;

        mark = false;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}

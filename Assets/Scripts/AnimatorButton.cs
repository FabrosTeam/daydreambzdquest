﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorButton : MonoBehaviour {
    Animator animator;
	// Use this for initialization
	void Start () {
        animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	public void ButtonOne()
    {
        animator.SetTrigger("Button1");
    }
    public void ButtonTwo()
    {
        animator.SetTrigger("Button2");
    }
    public void ButtonThree()
    {
        animator.SetTrigger("Button3");
    }
}
